from selenium import webdriver 
from selenium.webdriver.common.by import By 
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC 
from selenium.common.exceptions import TimeoutException
from bs4 import BeautifulSoup
import hashlib
import getpass
import base64
from Crypto import Random
from Crypto.Cipher import AES

BLOCK_SIZE = 32
enc_sbi_password = b'tTJar8Zois/rXmSSOUaVuJEcFHV9fdV+AjV2djs9jKgyC5XF0P0OWwc1jnY/Abks'
enc_sbi_username = b'C0YRSPH+M17jYkzGPDkWBYx8jo6CmrLmEfrSGna3u5Dwr7/+LtVzpp6o5Ydw7zNs'

def _pad(s):
    return s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * chr(BLOCK_SIZE - len(s) % BLOCK_SIZE)

def _unpad(s):
        return s[:-ord(s[len(s)-1:])]

def prepare_key(key): 
    return hashlib.sha256(key.encode()).digest()

def encrypt(raw, key):
    raw = _pad(raw)
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(prepare_key(key), AES.MODE_CBC, iv)
    return base64.b64encode(iv + cipher.encrypt(raw))

def decrypt(enc, key):
    enc = base64.b64decode(enc)
    iv = enc[:AES.block_size]
    cipher = AES.new(prepare_key(key), AES.MODE_CBC, iv)
    return _unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')


option = webdriver.ChromeOptions()
option.add_argument(" — incognito")

browser = webdriver.Chrome(executable_path='/sware/chromedriver/chromedriver', chrome_options=option)
browser.get("https://retail.onlinesbi.com/retail/login.htm")
browser.find_element_by_css_selector('.login_button').click()

try:
    dec_password = getpass.getpass()
except Exception as err:
   print('ERROR:', err)
else:
    print('Input password done')

username = decrypt(enc_sbi_username, dec_password)
password = decrypt(enc_sbi_password, dec_password)

user_field = browser.find_element_by_name('userName')
user_field.send_keys(username)

password_field = browser.find_element_by_name('password')
password_field.send_keys(password)

username = None
password = None
dec_password = None

browser.find_element_by_id('Button2').click()
browser.find_element_by_link_text('Account Statement').click()
browser.find_element_by_id('drd1').click()
browser.find_element_by_id('bymonth').click()
browser.find_element_by_id('Submit3').click()

html_source = browser.page_source
soup = BeautifulSoup(html_source, 'html.parser')
browser.find_element_by_link_text("LOGOUT").click()
browser.close()

data = []
table = soup.find('table', attrs={'class':'table table-hover table-bordered content_table table-striped'})
table_body = table.find('tbody')

rows = table_body.find_all('tr')
for row in rows:
    cols = row.find_all('td')
    cols = [ele.text.strip() for ele in cols]
    data.append([ele for ele in cols if ele])
